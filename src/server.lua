local class = require '30log'
local socket = require 'socket'

--- TODO
-- Las evaluaciones de los intrumentos deberia ser ascincronica ya que muchas van a leer archivos

local HTTP_NOT_FOUND = 'HTTP/1.0 404 Not Found\r\nServer: lua-metrics\r\nContent-Type: text/plain\r\n\r\nERROR: File Not Found.'
local HTTP_OK_HEADER = 'HTTP/1.0 200 OK\r\nServer: lua-metrics\r\nContent-Type: text/plain; version=0.0.4\r\n\r'

local Server = class('Server')

--- Init a ligth web server
-- @param port[opt=9100] Port to listening
-- @param address[opt='*'] Interfase to use. By default the system binds to all local interfaces using the INADDR_ANY
-- @param enpoint[opt='/metrics'] Server endpoint
-- @raise Cant start server on port. Probably already on use
function Server:init(port, address, endpoint)
  self.port = port or '9100'
  self.address = address or '*'
  self.endpoint = endpoint or '/metrics'
  self.instruments = {}

  self.server =  socket.bind(self.address, self.port)

  if not self.server then
    error(string.format('Cant start server on port %s. Probably already on use',
    self.port), 2)
  end

  print(string.format('Server listening on address: %s Port: %s',
  self.address, self.port))
end

--- Add some instrument to the server
-- @param instrument Intrument class instance
function Server:add_instrument(instrument)
  self.instruments[#self.instruments + 1] = instrument
end

--- Start to listening
function Server:start()
  while 1 do
    client = self.server:accept()
    client:settimeout(60)
    local request, err = client:receive()

    io.write(request, '\n')

    local query = request:match('^GET ' .. self.endpoint .. ' HTTP/[12]%.[01]$')
    if query == nil then
      client:send(HTTP_NOT_FOUND)
      client:close()
    else
      local response = ''
      for _, instrument in pairs(self.instruments) do
        instrument:read()
        response = response .. tostring(instrument)
      end

      client:send(HTTP_OK_HEADER .. '\n' .. response)
      client:close()
    end
  end
end

return Server
