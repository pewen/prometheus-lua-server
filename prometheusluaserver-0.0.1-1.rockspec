package = "PrometheusLuaServer"
version = "0.0.1-1"
source = {
   url = "..." -- We don't have one yet
}
description = {
   summary = "Simple and pure Lua server for Prometheus client library.",
   detailed = [[
      Tiny webserver using Lua socket to Prometheus Lua client library.
   ]],
   license = "GPLv3",
   homepage = "https://gitlab.com/fnbellomo/prometheus-lua-server"
}
dependencies = {
   "lua >= 5.1, <= 5.4",
   "30log >= 1.3.0",
   "luasocket"
}
build = {
  type = "builtin",
  modules = {
      ["prometheusServer"] = "src/server.lua"
  }
}
