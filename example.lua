local client = require 'prometheusClient'
local Server = require 'prometheusServer'


-- Init some collectors
-- Uptime from /proc/uptime
local uptime = client.collectors.uptime()

-- Meminfo from /proc/meminfo
-- Optional, you can pass with fields read
local fields = { 'MemFree', 'MemAvailable', 'SwapFree' }
local mem_info = client.collectors.mem_info(fields)

-- Load avg from /proc/loadavg
local loadavg = client.collectors.loadavg()

-- Packages per interface from /proc/net/dev
local packages_per_interface = client.collectors.packages_per_interface()

-- Init the server and add the instruments
server = Server(8001)
server:add_instrument(uptime)
server:add_instrument(mem_info)
server:add_instrument(loadavg)
server:add_instrument(packages_per_interface)
server:start()
