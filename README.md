# Prometheus Lua Server

Little server writer in Lua (using luasocket) to [Prometheus Lua Client](https://gitlab.com/pewen/prometheusluaclient)

## Install

```shell
$ luarocks install PrometheusLuaUhttpd
$ luarocks install PrometheusLuaClient
```

## Use

```lua
local client = require 'prometheusClient'
local Server = require 'prometheusServer'

-- Init some collectors
-- Uptime from /proc/uptime
local uptime = client.collectors.uptime()

-- Load avg from /proc/loadavg
local loadavg = client.collectors.loadavg()

-- Packages per interface from /proc/net/dev
local packages_per_interface = client.collectors.packages_per_interface()

-- Init the server and add the instruments
server = client.Server(8001)
server:add_instrument(uptime)
server:add_instrument(loadavg)
server:add_instrument(packages_per_interface)
server:start()
```

Init a small server. By default the server work on port 9100 and on `/metrics`. You can add as many instruments as you want.
